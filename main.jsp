<meta http-equiv="content-Type" content="text/html; charset=utf-8" />
<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import = "java.sql.*,javax.sql.*,java.io.*,java.util.*,java.text.*" %>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
<%
	TimeZone tz;
	java.util.Date date = new java.util.Date();
	DateFormat df = new SimpleDateFormat("yyyy년MM월dd일hh시mm분");
	tz = TimeZone.getTimeZone("Asia/Seoul"); df.setTimeZone(tz);
	String current=df.format(date);
%>
 <%
	 String myName = current;
	 Cookie cookieName = new Cookie("name", myName);
	 cookieName.setMaxAge(-1);
	 response.addCookie(cookieName);
 %>
</head>

<body>
<center>
<h2>Welecome To SKY RESORT.<br>
FREEDOM in PARADISE!!</h2>

<div class="w3-content w3-section" style="max-width:500px">
  <img class="mySlides" src="./photo/main1.jpg" style="width:100%">
  <img class="mySlides" src="./photo/main2.jpg" style="width:100%">
  <img class="mySlides" src="./photo/main3.jpg" style="width:100%">
</div>
<!--이미지 슬라이드를 위한 설정-->
<script>
var myIndex = 0;
carousel();

function carousel() {
  var i;
  var x = document.getElementsByClassName("mySlides");
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";  
  }
  myIndex++;
  if (myIndex > x.length) {myIndex = 1}    
  x[myIndex-1].style.display = "block";  
  setTimeout(carousel, 3000); // Change image every 2 seconds
}
</script>

<%
Cookie[] cookies = request.getCookies();
boolean reault = false;
try{
	for(int i=0; i<cookies.length; i++)
	{
		Cookie thisCookie = cookies[i];
		if("name".equals(thisCookie.getName()))
		{
			out.println("최근 방문일: "+thisCookie.getValue()+"<br>");
		}	
	}
}catch(Exception e){
	out.println("첫 방문입니다.");
}
%>
</center>
</body>
</html>