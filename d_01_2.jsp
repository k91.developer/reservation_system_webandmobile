<meta http-equiv="content-Type" content="text/html; charset=utf-8" />
<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import = "java.sql.*,javax.sql.*,java.io.*,java.util.*,java.text.*" %>
<!DOCTYPE html>
<html>
<head>
<!-- mystyle.css파일 import하여 적용-->
<link rel="stylesheet" type="text/css" href="mystyle2.css">	
</head>
<body>
<center>
<!--날짜와 요일을 가져오기 위한-->
<%
	Calendar cal = Calendar.getInstance();
	SimpleDateFormat dformat = new SimpleDateFormat("yyyy-MM-dd", Locale.KOREAN);
	SimpleDateFormat eformat = new SimpleDateFormat("(E)", Locale.KOREAN);
	String today=dformat.format(cal.getTime());
%>
<table border="0" cellspacing="0" width="60%">
<tr>
	<th>날짜</th>
	<th>풀빌라</th>
	<th>스위트</th>
	<th>디럭스</th>
</tr>
<%
String[][] resv_arr= new String[5][30];
// 2차원배열에 값 할당
for(int i=0; i<30; i++){
	resv_arr[0][i]=dformat.format(cal.getTime());
	resv_arr[1][i]=eformat.format(cal.getTime());
	cal.add(cal.DATE,+1);
}	// 객체생성
	Class.forName("com.mysql.jdbc.Driver");
	// DB연결
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/kopoctc", "root", "1234");
	// stmt 날짜별 객실별 예약고객 조회
	Statement stmt = conn.createStatement();
	ResultSet rs = stmt.executeQuery("select resv_date, max( if(room=1,name,0)), max(if(room=2,name,0)), max(if(room=3,name,0)) "+
									 "from joaresv where resv_date>="+today+" group by resv_date;");
	int k=0;
	// 각 배열에 값 할당
	while(rs.next()){
		resv_arr[2][k]=rs.getString(2);
		resv_arr[3][k]=rs.getString(3);
		resv_arr[4][k]=rs.getString(4);
		k++;
	}
	// 닫기
	rs.close();
	stmt.close();
	conn.close();

	// 화면 출력
	for(int i=0; i<30; i++){
		%>
		<tr>
		<td>
		<%
		for(int m=0; m<2; m++){
			// 주말일 경우 색상 변경
			if(resv_arr[1][i].equals("(토)")||resv_arr[1][i].equals("(일)")){
				%>
				<font color="red"><%=resv_arr[m][i]%>
				<%
			}else{
				%>
				<%=resv_arr[m][i]%>
			<%
			}
		}
		%>
		</td>
		<%
		for(int j=2; j<5; j++){
			// 예약이 가능한 곳(null)은 예약 가능으로 DB값 변경 및 태그 생성
			if(resv_arr[j][i]==null){
				%>
				<td><a href="d_02.jsp?date=<%=resv_arr[0][i]%>&date=<%=resv_arr[j][i]%>">예약가능</a></td>
				<%
			}else{
				%>
				<td><%=resv_arr[j][i]%></td>
				<%
			}
		}
		%>
		</tr>
		<%
	}	
%>
</table>
</body>
</html>
	