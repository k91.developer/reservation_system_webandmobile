<%@page contentType="text/html; charset=utf-8" language="java"%>
<%@page import="java.util.*, java.text.*"%>
<%@ page import = "java.sql.*,javax.sql.*,java.io.*,java.util.*,java.text.*" %>
<html>
<head>
<!-- mystyle.css파일 import하여 적용-->
<link rel="stylesheet" type="text/css" href="mystyle2.css">	
<!-- 로그인 여부 확인 -->
<% 
String loginOK=null;
String jumpURL="adm_login.jsp?jump=adm_allview.jsp";

loginOK=(String)session.getAttribute("login_ok");
// 로그인 안했을 경우 adm_login.jsp로 이동
if(loginOK==null){
	response.sendRedirect(jumpURL);
	return;
}
if(!loginOK.equals("yes")){
	response.sendRedirect(jumpURL);
	return;
}
%>
</head>
<body>
<center>
<!--날짜와 요일을 가져오기 위한-->
<%
	Calendar cal = Calendar.getInstance();
	SimpleDateFormat dformat = new SimpleDateFormat("yyyy-MM-dd", Locale.KOREAN);
	SimpleDateFormat eformat = new SimpleDateFormat("(E)", Locale.KOREAN);	
%>
<table border="0" cellspacing="0" width="60%">
<tr>
	<th>날짜</th>
	<th>풀빌라</th>
	<th>스위트</th>
	<th>디럭스</th>
</tr>
<%
// 2차원 배열 생성
String[][] resv_arr= new String[5][30];
// 2차원배열에 값 할당
for(int i=0; i<30; i++){
	resv_arr[0][i]=dformat.format(cal.getTime());
	resv_arr[1][i]=eformat.format(cal.getTime());
	cal.add(cal.DATE,+1);
	resv_arr[2][i]="1";
	resv_arr[3][i]="2";
	resv_arr[4][i]="3";	
}
	// 객체생성
	Class.forName("com.mysql.jdbc.Driver");
	// DB연결
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/kopoctc", "root", "1234");
	// stmt 풀빌라 예약 고객 조회
	Statement stmt = conn.createStatement();
	// stmt2 스위트 예약 고객 조회
	Statement stmt2 = conn.createStatement();
	// stmt3 디럭스 예약 고객 조회
	Statement stmt3 = conn.createStatement();
	
	// 화면 출력
	for(int i=0; i<30; i++){
	String reserve_1="예약가능";
	String reserve_2="예약가능";
	String reserve_3="예약가능";
	ResultSet rs = stmt.executeQuery("select * from joaresv where resv_date='"+resv_arr[0][i]+"' and room= "+resv_arr[2][i]+";");
	while(rs.next()){
		reserve_1 = rs.getString(1);
	}
	ResultSet rs2 = stmt2.executeQuery("select * from joaresv where resv_date='"+resv_arr[0][i]+"' and room= "+resv_arr[3][i]+";");
	while(rs2.next()){
		reserve_2 = rs2.getString(1);
	}
	ResultSet rs3 = stmt3.executeQuery("select * from joaresv where resv_date='"+resv_arr[0][i]+"' and room= "+resv_arr[4][i]+";");
	while(rs3.next()){
		reserve_3 = rs3.getString(1);
	}
	%>
	<tr>
		<td> <!-- 날짜(요일) 출력 -->
		<%
		for(int j=0; j<2; j++){
			if(resv_arr[1][i].equals("(토)")||resv_arr[1][i].equals("(일)")){
				out.println("<font color='red'>"+resv_arr[j][i]);
			}else{
			out.println(resv_arr[j][i]);
			}
		}
		%>
		</td>
		<td> <!-- 풀빌라 출력 -->
		<% // 예약한 사람이 없을 경우	
			if(reserve_1.equals("예약가능")){
			%>
			<a href="adm_oneview.jsp?dates=<%=resv_arr[0][i]%>&type=<%=resv_arr[2][i]%>"><%=reserve_1%></a>
			<%
			}else{ // 예약 가능할 경우
			%>	<a href="adm_oneview.jsp?dates=<%=resv_arr[0][i]%>&type=<%=resv_arr[2][i]%>"><%=reserve_1%></a>
			<%
			}
		%>
		</td>
		<td> <!-- 스위트 출력 -->
		<%	// 예약한 사람이 없을 경우	
			if(reserve_2.equals("예약가능")){
			%>
			<a href="adm_oneview.jsp?dates=<%=resv_arr[0][i]%>&type=<%=resv_arr[3][i]%>"><%=reserve_2%></a>
			<%
			}else{	// 예약 가능할 경우
			%>	<a href="adm_oneview.jsp?dates=<%=resv_arr[0][i]%>&type=<%=resv_arr[3][i]%>"><%=reserve_2%></a>
			<%
			}
		%>
		</td>
		<td> <!-- 디럭스 출력 -->
		<%	// 예약한 사람이 없을 경우	
			if(reserve_3.equals("예약가능")){
			%>
			<a href="adm_oneview.jsp?dates=<%=resv_arr[0][i]%>&type=<%=resv_arr[4][i]%>"><%=reserve_3%></a>
			<%
			}else{	// 예약 가능할 경우
			%>	<a href="adm_oneview.jsp?dates=<%=resv_arr[0][i]%>&type=<%=resv_arr[4][i]%>"><%=reserve_3%></a>
			<%
			}
		%>
		</td>
	</tr>
	<%
}
%>
</table>
</center>
</body>
</html>