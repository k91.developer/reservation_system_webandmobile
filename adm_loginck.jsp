<%@page import="java.sql.*, javax.sql.*, java.net.*, java.io.*"%>
<%@page contentType="text/html; charset=utf-8" language="java"%>
<html>
<head>
<!-- mystyle.css파일 import하여 적용-->
<link rel="stylesheet" type="text/css" href="mystyle2.css">	
</head>
<body>
<center>
<%	// admin_login.jsp에서 값을 가져오기 위한 파라미터
	request.setCharacterEncoding("utf-8");
	String jump = request.getParameter("jump");
	String id = request.getParameter("id");
	String pass = request.getParameter("passwd");
	
	boolean bPassChk=false;
	//아이디, 패스워드가 admin인지 체크
	if(id.replaceAll(" ","").equals("admin")&&pass.replaceAll(" ","").equals("admin"))
	{
		bPassChk=true;
	}else{
		bPassChk=false;
	}
		
	if(bPassChk){
		// 세션 추가
		session.setAttribute("login_ok","yes");
		session.setAttribute("login_id",id);
		response.sendRedirect(jump);
	}else{
		// 아이디,패스워드 틀릴 경우 오류 알림
		out.println("<h2>아이디 또는 패스워드 오류</h2>");
		out.println("<input type=button value='메인페이지' onclick=\"location.href='adm_login.jsp?jump="+jump+"'\">");
	}
%>
</center>
</body>
</html>