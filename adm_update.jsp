<meta http-equiv="content-Type" content="text/html; charset=utf-8" />
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*, javax.sql.*,java.io.*" %>
<%@ page import="java.util.*, java.text.*"  %>
<html>
<head>
<!-- mystyle.css파일 import하여 적용-->
<link rel="stylesheet" type="text/css" href="mystyle.css">
<%	// 한글 깨짐 방지
	request.setCharacterEncoding("UTF-8");
	// d_02.jsp에서 값 가져오기 위한 파라미터
	String name = request.getParameter("name");
	String date = request.getParameter("date");
	int room;
	String rooms = request.getParameter("room");
	room=Integer.parseInt(rooms);
	String addr = request.getParameter("addr");
	String telnum = request.getParameter("telnum");
	String in_name = request.getParameter("in_name");
	String comment = request.getParameter("comment");	
	String today = request.getParameter("today");		// form에서의 값들 , today는 작성일
	
	String dates = request.getParameter("real_date");	// 기존 날짜
	String type = request.getParameter("real_room");	// 기본 객실
%>
</head>
<body>
<%	// 객체생성
	Class.forName("com.mysql.jdbc.Driver");
	// DB연결
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/kopoctc", "root", "1234");
	
	if(dates.equals(date)&&type.equals(rooms)){
	// 같은날, 같은 객실에서 정보 수정
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery("select count(name) from joaresv where resv_date='"+date+"' and room="+room+";");
		rs.next();
		int rows=rs.getInt(1);
		rs.close();
		stmt.close();
		if(rows==0){
		// 관리자가 신규 예약시
			Statement stmt2 = conn.createStatement();
			stmt2.execute("insert into joaresv (name, resv_date, room, addr, telnum, in_name, comment, write_date,processing) "+
					 "values ('"+name+"','"+date+"',"+rooms+",'"+addr+"','"+telnum+"','"+in_name+"','"+comment+"','"+today+"',1);");
			stmt2.close();
			%> <!-- 기존예약자가 존재할 경우-->
				<script type="text/javascript">
				alert("예약이 수정되었습니다.");
				window.location.href="adm_allview.jsp";
				</script>
			<%			
		}else{
		// 관리자가 객실,날짜 외 정보 수정시
			Statement stmt2 = conn.createStatement();
			stmt2.execute("update joaresv set name='"+name+"',addr='"+addr+"', telnum='"+telnum+"', in_name='"+in_name+"', "+
						  "comment='"+comment+"', write_date='"+today+"' where resv_date='"+date+"' and room="+rooms+";");
			stmt2.close();
			%> <!-- 예약이 수정 경우-->
				<script type="text/javascript">
				alert("예약이 수정되었습니다.");
				window.location.href="adm_allview.jsp";
				</script>
			<%
		}
	}else{
	// 다른날짜 예약시 혹은 같은날짜 다른 객실 예약시
		Statement stmt = conn.createStatement();
		ResultSet rs = stmt.executeQuery("select count(name) from joaresv where resv_date='"+date+"' and room="+rooms+";");
		rs.next();
		int rows=rs.getInt(1);
		rs.close();
		stmt.close();
		if(rows==0){
			Statement stmt2 = conn.createStatement();
			Statement stmt3 = conn.createStatement();
			stmt2.execute("insert into joaresv (name, resv_date, room, addr, telnum, in_name, comment, write_date,processing) "+
					 "values ('"+name+"','"+date+"',"+rooms+",'"+addr+"','"+telnum+"','"+in_name+"','"+comment+"','"+today+"',1);");
			stmt3.execute("delete from joaresv where resv_date='"+dates+"' and room="+type+";");
			stmt3.close();
			stmt2.close();
			%> <!-- 예약이 수정 경우-->
				<script type="text/javascript">
				alert("예약이 수정되었습니다.");
				window.location.href="adm_allview.jsp";
				</script>
			<%
		}else{
		// 기존예약자가 존재할 경우 알림창
			%> <!-- 기존예약자가 존재할 경우-->
				<script type="text/javascript">
				alert("기존 예약자가 존재합니다.");
				window.location.href="adm_allview.jsp";
				</script>
			<%
		}
		
	}
	conn.close();
%>
<center>
<h2>고객 입력 정보</h2>
<table width="50%" border="1">
<tr>
	<th width="30%">이름</th>
	<td width="70%"><%=name%></td>
</tr>
<tr>
	<th>예약날짜</th>
	<td><%=date%></td>
</tr>
<tr>
	<th>객실</th>
	<td><%
	if(room==1){
		out.println("풀빌라");
	} else if(room==2){
		out.println("스위트");
	} else if(room==3){
		out.println("디럭스");
	}
	%></td>
</tr>
<tr>
	<th>주소</th>
	<td><%=addr%></td>
</tr>
<tr>
	<th>연락처</th>
	<td><%=telnum%></td>
</tr>
<tr>
	<th>입금자명</th>
	<td><%=in_name%></td>
</tr>
<tr>
	<th>남기실 말</th>
	<td><%=comment%></td>
</tr>
</table>
<table width="50%" border="0">
<tr>
	<td colspan="2"><h2>감사합니다. <br> 고객님의 예약이 완료되었습니다.</h2></td>
</tr>
<tr>
	<td><input type="button" value="예약상황" onclick="window.location.href='d_01.jsp'"></td>
</tr>
</table>
</center>
</body>
</html>