<meta http-equiv="content-Type" content="text/html; charset=utf-8" />
<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import = "java.sql.*, javax.sql.*,java.io.*" %>

<html>
<head>
<title>KSW01_게시판_RESORT</title>
<!-- mystyle.css파일 import하여 적용-->
<link rel="stylesheet" type="text/css" href="mystyle.css">
<!-- SQL문 오류코드를 알아내기 위한-->
<%!	
	public static int extractErrorCode (SQLException sqlException) {
		int errorCode = sqlException.getErrorCode();
		SQLException nested = sqlException.getNextException();
		while(errorCode==0&&nested!=null){
			errorCode=nested.getErrorCode();
			nested=nested.getNextException();
		}
		return errorCode;
	}
%>
</head>
<body>
<center>
<table id="top" class="mobile non-mobile" cellspacing=0>
	<tr>
		<td><h1>공 지 사 항</h1></td>
	</tr>
</table>
<%	// 객체 생성
	Class.forName("com.mysql.jdbc.Driver");
	// DB 연결
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/kopoctc", "root", "1234");
	// 테이블의 데이터를 가져오기 위한 SQL문
	Statement stmt = conn.createStatement();
	// 데이터가 존재 유무을 확인하기 위한 SQL문
	Statement stmt2 = conn.createStatement();
	ResultSet rset = stmt.executeQuery("select * from hotel_notice order by id desc;");
	ResultSet rs = stmt2.executeQuery("select count(*) from hotel_notice;");
	int rows=0;
	while (rs.next()){
		rows = rs.getInt(1);
	}
	%>
	<table class="mobile non-mobile" cellspacing=0 border=1>
	<tr>
	<th width="10%" align="center"> 번호 </th>
	<th width="70%" align="center"> 제목 </th>
	<th width="20%" align="center"> 등록일 </th>
	</tr>
	<%	// 데이터가 존재할 경우
	if(rows!=0){
		while (rset.next()) {
			%>
			<tr>
			<td><%=rset.getInt(1)%></td>
			<td><a href=view.jsp?cnt=<%=rset.getInt(1)%>><%=rset.getString(2)%></td>
			<td><%=rset.getDate(3)%></td>
			</tr>
			<%
		}
		%>
		</table>
	<%	
	}else{
	// 데이터가 존재하지 않을 경우
		%>
		<tr>
		<td colspan=5><p align=center>게시글이 없음</p></td>
		</tr>
		<%
	}
	%>
	</table>
	<%
	if(session.getAttribute("login_ok")!=null){
	%>
		<table class="mobile non-mobile" cellspacing=0 border=0>
		<tr><form action=insert.jsp>
		<td id='end'><input type=submit value=신규></td>
		</form></tr>
	<%
	}
	rs.close();
	rset.close();
	stmt2.close();
	stmt.close();
	conn.close();
%>
</center>
</div>
</body>
</html>