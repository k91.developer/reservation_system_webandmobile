<meta http-equiv="content-Type" content="text/html; charset=utf-8" />
<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import = "java.sql.*, javax.sql.*,java.io.*" %>
<html>
<head>
<title>KSW01_게시판_RESORT</title>
<!-- mystyle.css파일 import하여 적용-->
<link rel="stylesheet" type="text/css" href="mystyle.css">
<%! int cnt; %>
<%	// 한글깨짐 방지
	request.setCharacterEncoding("UTF-8");
	// 값을 받기 위한 파라미터
	String cnts = request.getParameter("cnt");
	cnt=Integer.parseInt(cnts);
%>
</head>
<body>
<center>
<%
	Class.forName("com.mysql.jdbc.Driver");
	Connection conn=DriverManager.getConnection("jdbc:mysql://localhost/kopoctc","root","1234");
	Statement stmt=conn.createStatement();
	ResultSet rs=stmt.executeQuery("select * from hotel_notice where id = " + cnt + ";");
%>
<table class="mobile non-mobile" id="top" cellspacing=0>
		<tr>
		<td><h1>게시글 보기</h1></td>
		</tr>
</table>
<table class="mobile non-mobile" cellspacing="0" border="1">
<%
	while(rs.next()){
		%>
		<tr>
		<th width='15%'>번호</th>
		<td class='view' width='85%'><%=rs.getInt(1)%></td>
		</tr><tr>
		<th>제목</th>
		<td class='view'><%=rs.getString(2)%></td>
		</tr><tr>
		<th>일자</th>
		<td><%=rs.getDate(3)%></td>
		</tr><tr>
		<th>내용</th>
		<td class='view'><%=rs.getString(4)%></td>
		</tr>
		<%
	}
%>
</table>
<table class="mobile non-mobile" cellspacing="0">
<tr>
	<td id="end" >
	<input type="button" value="목록" onclick="window.location.href='list.jsp'">
	<%
	if(session.getAttribute("login_ok")!=null){
	%>
	<input type="button" value="수정" onclick="window.location.href='update.jsp?cnt=<%=cnt%>'">
	<%
	}
	%>
	</td>
</tr>
</table>
</center>
</body>
</html>