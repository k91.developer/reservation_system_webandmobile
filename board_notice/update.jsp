<meta http-equiv="content-Type" content="text/html; charset=utf-8" />
<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import = "java.sql.*, javax.sql.*,java.io.*" %>
<html>
<head>
<title>KSW01_게시판_RESORT</title>
<!-- mystyle.css파일 import하여 적용-->
<link rel="stylesheet" type="text/css" href="mystyle.css">
<%
	request.setCharacterEncoding("UTF-8");
	
	int cnt;
	String cnts = request.getParameter("cnt");
	cnt=Integer.parseInt(cnts);
%>
<script>
<!-- 특수문자 입력을 막기 위한 함수 -->
function checkNumber(){
var objEv = event.srcElement;
var num ="{}[]()<>?_|~`!@#$%^&*-+\"'\\/";    //입력을 막을 특수문자 기재.
event.returnValue = true;
  
for (var i=0;i<objEv.value.length;i++) {
	 if(-1 != num.indexOf(objEv.value.charAt(i))){
		event.returnValue = false;
	}
}
if (!event.returnValue){
	alert("특수문자는 입력하실 수 없습니다.");
	var a="";
	for (var i=0;i<objEv.value.length-1;i++){
		a+=objEv.value.charAt(i);
	}
	objEv.value=a;
}
}
</script>
</head>
<body>
<center>
<%	// 객체 생성
	Class.forName("com.mysql.jdbc.Driver");
	// DB연결
	Connection conn=DriverManager.getConnection("jdbc:mysql://localhost/kopoctc","root","1234");
	// 해당 id의 데이터를 가져오기 위한 SQL문
	Statement stmt=conn.createStatement();
	ResultSet rs=stmt.executeQuery("select * from hotel_notice where id = " + cnt + ";");
%>

<table id="top" cellspacing=0>
		<tr>
		<td><h1></h1></td>
		</tr>
</table>
<table cellspacing="0" border="1">
<form method=post action=write.jsp>
	<%
	while(rs.next()){
		%>
		<tr>");
		<th width='10%'>번호</th>
		<td width='90%'><input type=number readonly name=cnt value=<%=rs.getInt(1)%>></input></td>
		</tr><tr>
		<th>제목</th>
		<td><input type=text size=35 onKeyDown='checkNumber();' pattern='^[\\S].+' required name=title value=<%=rs.getString(2)%>></td>
		</tr><tr>
		<th>일자</th>
		<td><%=rs.getDate(3)%></td>
		</tr><tr>
		<th>내용</th>
		<td><textarea onKeyDown='checkNumber();' required name=content><%=rs.getString(4)%></textarea></td>
		</tr>
		<%
	}
%>
</table>
<table cellspacing="0">
<tr>
	<td id="end">
	<input type="button" class="button" value="취소" onclick="window.location.href='list.jsp'">
	<input type="submit" value="쓰기">
	</form>
	<input type="button" class="button" value="삭제" onclick="window.location.href='delete.jsp?cnt=<%=cnt%>'">
	</td>
</tr>
</table>
<%
	rs.close();
	stmt.close();
	conn.close();
%>
</center>
</body>
</html>