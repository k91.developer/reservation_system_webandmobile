<meta http-equiv="content-Type" content="text/html; charset=utf-8" />

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*, javax.sql.*,java.io.*" %>
<html>
<head>
<title>KSW01_게시판_RESORT</title>
<!-- mystyle.css파일 import하여 적용-->
<link rel="stylesheet" type="text/css" href="mystyle.css">
<!--SQL오류코드를 알아내기 위한-->
<%!		
	public static int extractErrorCode (SQLException sqlException) {
		int errorCode = sqlException.getErrorCode();
		SQLException nested = sqlException.getNextException();
		while(errorCode==0&&nested!=null){
			errorCode=nested.getErrorCode();
			nested=nested.getNextException();
		}
		return errorCode;
	}
%>

<script>
<!-- 오늘 날짜를 알아내기 위한 -->
		var today = new Date();
		var dd = today.getDate();
		var mm = today.getMonth()+1; //January is 0!
		var yyyy = today.getFullYear();

		if(dd<10) {
			dd='0'+dd
		} 
		if(mm<10) {
			mm='0'+mm
		} 
		today = yyyy+'-'+mm+'-'+dd;
<!-- 특수문자 입력을 막기 위한 함수 -->
function checkNumber(){
var objEv = event.srcElement;
var num ="{}[]()<>?_|~`!@#$%^&*-+\"'\\/";    //입력을 막을 특수문자 기재.
event.returnValue = true;
  
for (var i=0;i<objEv.value.length;i++) {
	 if(-1 != num.indexOf(objEv.value.charAt(i))){
		event.returnValue = false;
	}
}
if (!event.returnValue){
	alert("특수문자는 입력하실 수 없습니다.");
	var a="";
	for (var i=0;i<objEv.value.length-1;i++){
		a+=objEv.value.charAt(i);
	}
	objEv.value=a;
}
}
</script>
</head>
<body>
<center>
	<table id="top" cellspacing=0>
		<tr>
			<td><h1></h1></td>
		</tr>
	</table>
	<form method=post action=write.jsp>
	<table border=1 cellspacing=0>
		<tr>
			<th width="10%">번호</th>
			<td width="90%" ><input type="hidden" value="" >신규(insert)</td>
		</tr>
		<tr>
			<th>제목</th>
			<td><input type="text" name="title" onKeyDown="checkNumber();" pattern="^[\S].+" required maxlength="70" placeholder="70자 이내로 작성하시오.(특수문자 제외)"></input></td>
		</tr>
		<tr>
			<th>일자</th>
			<td><script>document.write(today);</script></td>
		</tr>
		<tr>
			<th>내용</th>
			<td><textarea name="content" onKeyDown="checkNumber();" required maxlength="1000"></textarea></td>	
		</tr>
	</table>
	<table cellspacing=0>
		<tr>
			<td><input type="submit" value="쓰기">
	</form>	
				<input type="button" class="button" value="취소" onclick="location.href='list.jsp'">
			</td>
		</tr>
	</table>
</center>
</body>
</html>