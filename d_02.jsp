<meta http-equiv="content-Type" content="text/html; charset=utf-8" />
<%@ page contentType="text/html; charset=utf-8" %>
<%@ page import = "java.sql.*,javax.sql.*,java.io.*,java.util.*,java.text.*" %>
<!DOCTYPE html>
<html>
<head>
<!-- mystyle.css파일 import하여 적용-->
<link rel="stylesheet" type="text/css" href="mystyle.css">
<%! String dates; %>

<%	// 오늘 날짜
	Calendar cal = Calendar.getInstance();
	SimpleDateFormat dformat = new SimpleDateFormat("yyyy-MM-dd", Locale.KOREAN);
	String today= dformat.format(cal.getTime());
	// 29일 뒤 날짜
	cal.add(cal.DATE,+29);
	String month= dformat.format(cal.getTime());
%>
<%	// 한글깨짐 방지
	request.setCharacterEncoding("UTF-8");
	// 해당 id를 조회하기 위한 파라미터
	
	// 예약조회에서 예약하기로 이동할 경우
	int id;
	dates = request.getParameter("date");
	// 예약하기로 바로 들어올 경우 날짜는 오늘 날짜
	if(dates==null){
		dates=today;
	}
	int type;
	String types = request.getParameter("type");
	try{
		type=Integer.parseInt(types);
	}catch(Exception e){
		type=0;
	}
	
%>
<script language="javascript">
<!-- 필수 입력하도록 하는 함수 // 전화번호 양식 정규식 체크 -->
function writeCheck()
{
	var form = document.writeform;
	// form 에 있는 name 값이 없을 때 
	if( !form.name.value ) { 
		alert( "고객명을 적어주세요" );	// 경고창 띄움 
		form.name.focus(); // form 에 있는 name 위치로 이동 
		return; 
	}
	if( !form.addr.value ) { 
		alert( "주소를 적어주세요" ); 
		form.addr.focus(); 
		return; 
	} 
	if( !form.telnum.value ) { 
		alert( "연락처를 적어주세요" ); 
		form.telnum.focus(); 
		return; 
	}
	var regExp = /^\d{3}-\d{3,4}-\d{4}$/;
	if ( !regExp.test( form.telnum.value ) ) {

      alert("잘못된 휴대폰 번호입니다. 숫자, - 를 포함한 숫자만 입력하세요.");

      return;
	}
	if( !form.in_name.value ) { 
		alert( "입금자명을 적어주세요" ); 
		form.in_name.focus(); 
		return; 
	} 
	form.submit(); 
}

</script>
</head>
<body>
<center>
<form name="writeform" method="post" action="d_02_write.jsp"> 
<table border="1" cellspacing="0" width="80%">
<tr>
	<th>고객명</th>
	<td><input type="text" name="name" maxlength="20"></td>
</tr>
<tr>
	<th>예약일자</th> <!-- 오늘날짜부터 예약가능날짜까지만 선택가능하도록 제한-->
	<td><input type="date" name="date" value="<%=dates%>" min=<%=today%> max=<%=month%> name="resv_date"></td>
</tr>
<tr>
	<th>객실</th>
	<td>	<!--type으로 받은 값에 따라 기본값으로 설정-->
		<select name="room">
			<% if(type==1){
				%>
				<option value="1" selected>풀빌라</option>
				<%
			}else{
				%>
				<option value="1">풀빌라</option>
				<%			
			}
			if(type==2){
				%>
				<option value="2" selected>스위트</option>
				<%
			}else{
				%>
				<option value="2">스위트</option>
				<%			
			}
			if(type==3){
				%>
				<option value="3" selected>디럭스</option>
				<%
			}else{
				%>
				<option value="3">디럭스</option>
				<%			
			}
			%>
		</selct>
	</td>
</tr>
<tr>
	<th>주소</th>
	<td><input type="text" name="addr"></td>
</tr>
<tr>
	<th>연락처</th>
	<td><input type="tel" name="telnum"></td>
</tr>
<tr>
	<th>입금자명</th>
	<td><input type="text" name="in_name"></td>
</tr>
<tr>
	<th>남기실 말</th>
	<td><input type="text" name="comment"></td>
	<input type="hidden" name="today" value=<%=today%>>
</tr>
</table>
<table cellspacing="0" width="80%">
<tr>
	<td id="end"><input type="button" value="예약" onclick="javascript:writeCheck();"></td>
</tr>
</table>
</form>
</center>
</body>
</html>