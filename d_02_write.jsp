<meta http-equiv="content-Type" content="text/html; charset=utf-8" />
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import = "java.sql.*, javax.sql.*,java.io.*" %>
<%@ page import="java.util.*, java.text.*"  %>
<html>
<head>
<!-- mystyle.css파일 import하여 적용-->
<link rel="stylesheet" type="text/css" href="mystyle.css">
<%	// 한글 깨짐 방지
	request.setCharacterEncoding("UTF-8");
	// d_02.jsp에서 값 가져오기 위한 파라미터
	String name = request.getParameter("name");
	String date = request.getParameter("date");
	int room;
	String rooms = request.getParameter("room");
	room=Integer.parseInt(rooms);
	String addr = request.getParameter("addr");
	String telnum = request.getParameter("telnum");
	String in_name = request.getParameter("in_name");
	String comment = request.getParameter("comment");
	String today = request.getParameter("today");
%>
</head>
<body>
<%	// 객체생성
	Class.forName("com.mysql.jdbc.Driver");
	// DB연결
	Connection conn = DriverManager.getConnection("jdbc:mysql://localhost/kopoctc", "root", "1234");
	Statement stmt = conn.createStatement();
	// 바로 예매로 들어와 했을 때의 오류 발생 예방
	try{	
		stmt.execute("insert into joaresv (name, resv_date, room, addr, telnum, in_name, comment, write_date,processing) "+
					 "values ('"+name+"','"+date+"',"+room+",'"+addr+"','"+telnum+"','"+in_name+"','"+comment+"','"+today+"',1);");
	}catch(Exception e){
	// 예약 불가능할 때 경고창
		%>
		<script type="text/javascript">
		alert("예약이 불가능합니다. 다른 날짜나 다른 객실을 선택하세요.");
		window.location.href="d_01.jsp";
		</script>
		<%
	}
%>
<center>
<h2>고객 입력 정보</h2>
<table width="50%" border="1">
<tr>
	<th width="30%">이름</th>
	<td width="70%"><%=name%></td>
</tr>
<tr>
	<th>예약날짜</th>
	<td><%=date%></td>
</tr>
<tr>
	<th>객실</th>
	<td><%
	if(room==1){
		out.println("풀빌라");
	} else if(room==2){
		out.println("스위트");
	} else if(room==3){
		out.println("디럭스");
	}
	%></td>
</tr>
<tr>
	<th>주소</th>
	<td><%=addr%></td>
</tr>
<tr>
	<th>연락처</th>
	<td><%=telnum%></td>
</tr>
<tr>
	<th>입금자명</th>
	<td><%=in_name%></td>
</tr>
<tr>
	<th>남기실 말</th>
	<td><%=comment%></td>
</tr>
</table>
<table width="50%" border="0">
<tr>
	<td colspan="2"><h2>감사합니다. <br> 고객님의 예약이 완료되었습니다.</h2></td>
</tr>
<tr>
	<td><input type="button" value="예약상황" onclick="window.location.href='d_01.jsp'"></td>
</tr>
</table>
</center>
</body>
</html>